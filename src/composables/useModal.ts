import { ref, Ref } from 'vue';

interface UseModal {
  title: Ref<string>;
  setTitle: (value: string) => void;
  isVisible: Ref<boolean>;
  visibilityOn: () => void;
  visibilityOff: () => void;
  error: Ref<boolean>;
  setError: (value: boolean) => void;
}

export default function useModal(): UseModal {
  const title = ref('');
  const setTitle = (value: string) => (title.value = value);

  const visibility = ref(false);
  const setVisibility = (value: boolean) => (visibility.value = value);

  const error = ref(false);
  const setError = (value: boolean) => (error.value = value);

  return {
    title,
    setTitle,
    isVisible: visibility,
    visibilityOn: () => setVisibility(true),
    visibilityOff: () => setVisibility(false),
    error,
    setError
  };
}
