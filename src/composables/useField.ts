import { ref, Ref } from 'vue';

export default function useField(initialState: string): [Ref<string>, (e: Event) => void] {
  const field = ref(initialState);

  const setField = (e: Event) => {
    field.value = (e.target as HTMLInputElement).value;
  };

  return [field, setField];
}
