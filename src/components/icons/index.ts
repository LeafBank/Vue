import Chip from './Chip.vue';
import MasterCard from './MasterCard.vue';
import NearFieldCom from './NearFieldCom.vue';
import Visa from './Visa.vue';

export { Chip, MasterCard, NearFieldCom, Visa };
