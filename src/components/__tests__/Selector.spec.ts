import { render, screen } from '@testing-library/vue';
import userEvent from '@testing-library/user-event';
import Selector from '../Selector.vue';

describe('<Selector />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(Selector);

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(Selector, {
      props: { defaultLabel: 'Hello World', ariaLabelledBy: 'helloWorld', options: ['Lorem Ipsum'] }
    });

    expect(screen.queryByText('Lorem Ipsum')).not.toBeInTheDocument();
  });

  it('Should Emit Events', async () => {
    const { emitted } = render(Selector, {
      props: { defaultLabel: 'Hello World', ariaLabelledBy: 'helloWorld', options: ['Lorem Ipsum'] }
    });

    const buttonElement = screen.getByText('Hello World');

    await userEvent.click(buttonElement);

    expect(screen.queryByText('Lorem Ipsum')).toBeInTheDocument();
    expect(emitted()).toHaveProperty('btn-click');
  });
});
