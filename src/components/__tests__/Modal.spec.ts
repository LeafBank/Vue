import { render } from '@testing-library/vue';
import Modal from '../Modal.vue';

describe('<Modal />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(Modal, {
      props: { isVisible: true, title: 'Hello World' }
    });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders As Resolved', () => {
    const { container, getByText } = render(Modal, {
      props: {
        isVisible: true,
        title: 'Hello World'
      }
    });

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(16, 185, 129);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
  });

  it('Should Renders As Rejected', () => {
    const { container, getByText } = render(Modal, {
      props: {
        isVisible: true,
        isError: true,
        title: 'Hello World'
      }
    });

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(239, 68, 68);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
  });
});
