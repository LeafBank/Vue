import { render, screen, fireEvent } from '@testing-library/vue';
import userEvent from '@testing-library/user-event';
import Field from '../Field.vue';

describe('<Field />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(Field, { props: { id: 'field' } });

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(Field, {
      props: {
        classes: 'row',
        style: 'margin: auto',
        id: 'field',
        name: 'field',
        label: 'Field',
        type: 'text',
        placeholder: 'Field',
        maxLength: 10
      }
    });

    expect(screen.queryByText('Field')).toBeInTheDocument();
    expect(screen.queryByPlaceholderText('Field')).toBeInTheDocument();
  });

  it('Should Emit Events', async () => {
    const { emitted } = render(Field, {
      props: {
        classes: 'row',
        style: 'margin: auto',
        id: 'field',
        name: 'field',
        label: 'Field',
        type: 'text',
        placeholder: 'Field',
        maxLength: 10
      }
    });

    const fieldElement = screen.getByPlaceholderText('Field');

    await fireEvent.focus(fieldElement);
    await fireEvent.update(fieldElement, 'Hello World');

    expect(screen.queryByPlaceholderText('Field')).toHaveValue('Hello World');
    expect(emitted()).toHaveProperty('input');
    expect(emitted()).toHaveProperty('focus');
  });
});
