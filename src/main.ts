import { createApp } from 'vue';
import App from './components/App.vue';
import './reset.css';
import './index.css';

createApp(App).mount('#root');
